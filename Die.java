import java.util.Random;
public class Die
{
	//Fields
	private int faceValue;
	private Random randNumGen;
	
	//Constructor
	public Die()
	{
		this.faceValue = 1;
		randNumGen = new Random();
	}
	
	//Standard Methods
	public String toString()
	{
		return getFaceValue() + "";
	}
	
	//Getters
	public int getFaceValue()
	{
		return this.faceValue;
	}
	
	//Instance Methods
	public void roll()
	{
		this.faceValue = randNumGen.nextInt(6)+1;
	}
	
}