public class Board
{
	//Fields
	private Die dice1;
	private Die dice2;
	boolean[] tiles;
	
	//Constructor
	public Board()
	{
		dice1 = new Die();
		dice2 = new Die();
		tiles = new boolean[12];
	}
	
	//Standard Methods
	public String toString()
	{
		String result = "";
		for (int i = 0; i < this.tiles.length; i++)
		{
			if (this.tiles[i])
			{
				result += (i+1) + " ";
			}
			else
			{
				result += "X ";
			}
		}
		//using substring to remove the extra space at the end of the String result
		return result.substring(0, result.length()-1);
	}
	
	//Instance Methods
	public boolean playATurn()
	{
		System.out.println("Rolling the dices...");
		dice1.roll();
		dice2.roll();
		System.out.println("Die #1: " + dice1 + "\t" + "Die #2: " + dice2);
		
		int diceValue1 = dice1.getFaceValue();
		int diceValue2 = dice2.getFaceValue();
		int sumOfDice = diceValue1 + diceValue2;
		
		if (!checkTiles(sumOfDice))
		{
			tiles[sumOfDice-1] = true;
			System.out.println("Closing tile equal to sum: " + sumOfDice);
			return false;
		}
		
		if (!checkTiles(diceValue1))
		{
			tiles[diceValue1-1] = true;
			System.out.println("Closing tile with same value as die #1: " + diceValue1);
			return false;
		}
		
		if (!checkTiles(diceValue2))
		{
			tiles[diceValue2-1] = true;
			System.out.println("Closing tile with same value as die #2: " + diceValue2);
			return false;
		}
		
		System.out.println("All the tiles for these values are already shut.");
		return true;
	}
	
	public void reset()
	{
		for (int i = 0; i < tiles.length; i++)
		{
			tiles[i] = false;
		}
	}
	
	//Private Helper Methods
	private boolean checkTiles(int numPos)
	{
		return this.tiles[numPos-1];
	}
	
}