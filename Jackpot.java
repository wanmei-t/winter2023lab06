import java.util.Scanner;
public class Jackpot
{
	public static void main(String[] args)
	{		
		//Welcome
		System.out.println("Welcome to the Jackpot game!");
		
		//Setup
		Scanner scan = new Scanner(System.in);
		int winCount = 0;
		int gameCount = 0;
		int newGame = 1;
		Board board = new Board();
		
		while (newGame == 1)
		{
			//Individual game setup
			board.reset();
			boolean gameOver = false;
			int numOfTilesClosed = 0;
			
			//Game
			while (!gameOver)
			{
				System.out.println("The tiles shut are: \n" + board);
				
				if (board.playATurn()) gameOver = true;
				else numOfTilesClosed ++;
				
				System.out.println();
			}
			
			//Announcing win or lost
			if (numOfTilesClosed >= 7)
			{
				System.out.println("You reached the jackpot with " + numOfTilesClosed + " tiles shut! Congratulations!");
				winCount++;
			}
			else
			{
				System.out.println("You lost with only " + numOfTilesClosed + " tiles shut...");
				System.out.println("Better luck next time!");
			}
			
			//Asking the user for another game
			System.out.println("Would you like to play another game? Answer 1 for one more game or 0 to end the game.");
			newGame = scan.nextInt();
			gameCount++;
		}
		
		System.out.println();
		System.out.println("Out of " + gameCount + " games, you winned " + winCount + " times. Good bye!");
	}
}